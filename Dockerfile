FROM ubuntu:16.04

MAINTAINER Pablo Ibarra <pablo.ibarras@alumnos.usm.cl>

LABEL Description="TSC"

RUN apt-get update

RUN apt-get install -y -q \
    curl

RUN curl -sL https://deb.nodesource.com/setup_6.x | bash -

RUN apt-get install -y -q \
    nodejs \
    && apt-get -y autoclean \
    && rm -rf /var/lib/apt/lists/*

RUN npm install -g ionic cordova


RUN echo '1. Run ineractively and map the /projects volume to your host, e.g. docker run -it -v /Users/<host_path>:/projects <image>' > /readme.txt
RUN echo '2. ionic start myFirstIonic2App sidemenu --v2 --ts ### --ts selects TypeScript' >> /readme.txt
RUN echo '3. cd myFirstIonic2App' >> /readme.txt
RUN echo '4. ionic serve --all' >> /readme.txt

RUN echo 'cd /projects' > /start.sh
RUN echo 'cat /readme.txt' >> /start.sh
RUN echo 'ionic serve --all' >> /start.sh

WORKDIR /projects

EXPOSE 8100 35729
