// Yaqoob, N. (2016, Mar 25). How to Deploy Hybrid Mobile App using Ionic Framework on Heroku [Blog post]. Retrieve from
// https://medium.com/@nomanyaqoob/how-to-deploy-hybrid-mobile-app-using-ionic-framework-on-heroku-889c2600805b
var express = require('express');
var path = require('path');
var app = express();
app.use(express.static(path.resolve(__dirname, "www")));
app.set('port', process.env.PORT || 8001);
app.listen(app.get('port'), function() {
 console.log("listening to Port", app.get("port"));
});
