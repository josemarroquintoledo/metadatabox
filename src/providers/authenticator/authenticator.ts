/**
 * @author jose@marroquin.cl (Jose David Marroquin Toledo)
 * @updated 2017-12-17
 */

import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Events } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import 'rxjs/add/operator/map';

/*
  Generated class for the AuthenticatorProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class AuthenticatorProvider {

  constructor(public http: Http,
              public events: Events,
              public afAuth: AngularFireAuth) {
    console.log('Hello AuthenticatorProvider Provider');
  }

  logIn(service: string) {
    return new Promise((resolve, reject) => {
      if (service == 'google') {
        this.afAuth.auth
          .signInWithPopup(new firebase.auth.GoogleAuthProvider())
          .then(res => {
            console.log("Logged into Google!");
            resolve(res);
          })
          .catch(err => {
            console.log("Error logging into Google " + err);
            reject();
        });
      } else if (service == 'facebook') {
        this.afAuth.auth
          .signInWithPopup(new firebase.auth.FacebookAuthProvider())
          .then(res => {
            console.log("Logged into Facebook!");
            resolve(res);
          })
          .catch(err => {
            console.log("Error logging into Facebook " + err);
            reject();
        });
      }
    });
  }

  logOut() {
    firebase.auth().signOut().then(() => {
      console.log("Sign-out successful.");
      this.events.publish('user:logged-out');
    }).catch(err => {
      console.log("An error happened trying to sign out: " + err);
    });
  }

  getCurrentUser() {
    return new Promise((resolve, reject) => {
      firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
          console.log("User is signed in.");
          let userProfile = {
            name: user.displayName,
            email: user.email,
            photoUrl: user.photoURL,
            emailVerified: user.emailVerified
          };
          resolve(userProfile);
        } else {
          console.log("No user is signed in.")
          reject();
        }
      });
    });
  }
}
