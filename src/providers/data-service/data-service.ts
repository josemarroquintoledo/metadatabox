import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

/*
  Generated class for the DataServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DataServiceProvider {
  private files: any;
  private API_URL = 'http://localhost:3000';

  constructor(public http: Http) {
    console.log('Hello DataServiceProvider Provider');
  }

  getFolderList(params?: {
    name?: Array<string>,
    id_parent_folder?: number,
    limit?: number,
    offset?: number
  }) {
    let paramsKey = ['name', 'id_parent_folder', 'limit', 'offset'];
    let obj = {};
    if (params) {
      for (let key of paramsKey) {
        if (params[key]) {
          obj[key] = params[key];
        }
      }
    }
    return new Promise((resolve, reject) => {
      this.doAPIRequest('folders', obj)
      .then(res => {
        resolve(res);
      })
      .catch(() => {
        reject();
      });
    });
  }

  makeFolder(params: {name: string, id_parent_folder: number}) {
    return new Promise((resolve, reject) => {
      this.doAPIRequest('folders', {
        "name": params.name,
        "id_parent_folder": params.id_parent_folder
      })
      .then(res => {
        resolve(res);
      })
      .catch(() => {
        reject();
      });
    });
  }

  getFolder(params: {id: number}) {
    return new Promise((resolve, reject) => {
      this.doAPIRequest('folders', {
        "id": params.id
      })
      .then(res => {
        resolve(res);
      })
      .catch(() => {
        reject();
      });
    });
  }

  changeFolderName(params: {name: string, id_parent_folder: number}) {
    return new Promise((resolve, reject) => {
      this.doAPIRequest('folders', {
        "name": params.name,
        "id_parent_folder": params.id_parent_folder
      })
      .then(res => {
        resolve(res);
      })
      .catch(() => {
        reject();
      });
    });
  }

  deleteFolder(params: {id: number, force: number}) {
    return new Promise((resolve, reject) => {
      this.doAPIRequest('folders', {
        "id": params.id,
        "force": params.force
      })
      .then(res => {
        resolve(res);
      })
      .catch(() => {
        reject();
      });
    });
  }

  doAPIRequest(type: string, params: any) {
    return new Promise((resolve, reject) => {
      let requestURL = this.API_URL;
      requestURL += '/' + type;  // '/folders' or '/files'.
      for (let key of Object.keys(params)) {
        let value = params[key];
        if (value) {
          requestURL += '&';
          requestURL += key;
          requestURL += '=';
          if (value instanceof Array) {
            for (let i = 0; i < value.length; i++) {
              requestURL += String(value[i]);
              if (i < value.length - 1) {
                requestURL += ',';
              }
            }
          } else {
            requestURL += params[key];
          }
        }
      }
      this.http.get(requestURL).map(res => res.json()).subscribe(data => {
        console.log("doAPIRequest's HTTP response: successful.");
        console.log(data);
        resolve(data);
      }, error => {
        console.log("doAPIRequest's HTTP response: " + error);
        reject();
      });
    });
  }

  getFileList() {
    return new Promise(resolve => {
      setTimeout(() => {
        console.log("Success: I got a file list.");
        resolve(this.files);
      }, 1500);
    });
  }
}