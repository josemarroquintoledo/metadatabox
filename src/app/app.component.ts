import { Component, ViewChild } from '@angular/core';
import { Nav,
         Platform,
         Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
// Pages
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
// Providers
import { AuthenticatorProvider } from '../providers/authenticator/authenticator';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  private userProfile: {
    name: string,
    email:string,
    photoUrl: string,
    emailVerified: boolean
  };
  private loggedInUser: boolean;
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform,
              public authService: AuthenticatorProvider,
              public events: Events,
              public statusBar: StatusBar,
              public splashScreen: SplashScreen) {
    this.initializeApp();
    this.subscribe();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage }
    ];

  }

  subscribe() {
    this.events.subscribe('user:logged-in', user => {
      console.log("'user:logged-in' was published!");
      this.userProfile = user;
      this.loggedInUser = true;
    });
    this.events.subscribe('user:logged-out', () => {
      console.log("'user:logged-out' was published!");
      this.loggedInUser = false;
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  goToLogin() {
    this.nav.push(LoginPage);
  }

  logOut() {
    this.authService.logOut();
  }
}
