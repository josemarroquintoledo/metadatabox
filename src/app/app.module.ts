import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { ObjectMenuPopover } from '../pages/home/home';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
// Providers
import { AuthenticatorProvider } from '../providers/authenticator/authenticator';
import { DataServiceProvider } from '../providers/data-service/data-service';
// Firebase
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { PipesModule }  from '../pipes/pipes.module';

export const firebaseConfig = {
    apiKey: "AIzaSyBhfbL32wc38Auaes7DChqb1_6qbtsCr9I",
    authDomain: "metadatabox-b20f4.firebaseapp.com",
    databaseURL: "https://metadatabox-b20f4.firebaseio.com",
    projectId: "metadatabox-b20f4",
    storageBucket: "metadatabox-b20f4.appspot.com",
    messagingSenderId: "274373511019"
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    ObjectMenuPopover
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule,
    PipesModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    ObjectMenuPopover
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthenticatorProvider,
    DataServiceProvider
  ]
})
export class AppModule {}
