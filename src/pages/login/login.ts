/**
 * @author jose@marroquin.cl (Jose David Marroquin Toledo)
 * @updated 2017-12-10
 */

import { Component } from '@angular/core';
import { IonicPage,
         NavController,
         PopoverController,
         ToastController,
         NavParams } from 'ionic-angular';
// Pages
import { HomePage } from '../home/home';
// Providers
import { AuthenticatorProvider } from '../../providers/authenticator/authenticator';

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  constructor(public navCtrl: NavController,
              public popoverCtrl: PopoverController,
              public navParams: NavParams,
              public toastCtrl: ToastController,
              public authService: AuthenticatorProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  presentLoginErrToast(serv: string) {
    let servName = serv.charAt(0).toUpperCase() + serv.slice(1);
    let errMessage = "Lo sentimos, no pudimos loguearte con " + servName + '.'
    const toast = this.toastCtrl.create({
      message: errMessage,
      duration: 3000,
      position: 'top',
      dismissOnPageChange: true
    });
    toast.present();
  }

  logIn(serv: string) {
    this.authService
      .logIn(serv)
      .then(() => {
        this.navCtrl.setRoot(HomePage);
      })
      .catch(() => {
        this.presentLoginErrToast(serv);
      });
  }
}
