import { Component } from '@angular/core';
import { NavController,
         AlertController,
         PopoverController,
         ViewController,
         Events } from 'ionic-angular';
// Providers
import { DataServiceProvider } from '../../providers/data-service/data-service';
import { AuthenticatorProvider } from '../../providers/authenticator/authenticator';

@Component({
  template: `
    <ion-list no-lines no-margin>
      <button ion-item detail-none (click)="presentPrompt()">
        <ion-icon name="create" item-left></ion-icon>
        Renombrar
      </button>
      <button ion-item detail-none (click)="close()">
        <ion-icon name="close" color="danger" item-left></ion-icon>
        <span ion-text color="danger">Eliminar</span>
      </button>
    </ion-list>
  `
})
export class ObjectMenuPopover {
  constructor(public viewCtrl: ViewController,
              private alertCtrl: AlertController) {

  }

  close() {
    this.viewCtrl.dismiss();
  }

  presentPrompt() {
    let alert = this.alertCtrl.create({
      title: 'Renombrar',
      message: 'Por favor, ingrese un nuevo nombre para este \u00edtem:',
      inputs: [
        {
          name: 'objectName'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'OK',
          handler: data => {
            return data;
          }
        }
      ]
    });
    alert.present();
  }
}

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  private folderList: any;
  private items: any;
  private searchBar: boolean;
  private nameArrowUp: boolean;
  private dateArrowUp: boolean;
  private lookingForFiles: boolean;
  private sortedByName: boolean;
  private rootFolderIDInThisView: string;
  private folderTree: any;
  private parent: any;

  constructor(public navCtrl: NavController,
              private alertCtrl: AlertController,
              public authService: AuthenticatorProvider,
              public events: Events,
              public popoverCtrl: PopoverController,
              public dataService: DataServiceProvider) {
    this.searchBar = false;
    this.dateArrowUp = false;
  }

  ionViewDidEnter() {
    this.authService
    .getCurrentUser()
    .then(res => {
      this.events.publish('user:logged-in', res);
      this.initFolderList();
    }).catch(() => {
      console.log("There isn't a logged in and authorized user to add or delete events.");
    });
  }

  buildTree() {
    // Builds the directory tree.
    this.folderTree = new Object();
    if (this.folderList) {
      for (let folder of this.folderList) {
        if (this.folderTree[folder._id] == undefined) {
          this.folderTree[folder._id] = new Array();
          if (this.folderTree[folder.parent_folder_id] == undefined) {
            this.folderTree[folder.parent_folder_id] = new Array();
          }
          // folder._id (string) isn't needed to be verified if it's in
          // folderTree[folder.parent_folder_id] (Array()) because ID are
          // unique.
          this.folderTree[folder.parent_folder_id].push(folder);
        }
      }
    }
    if (this.folderTree['-1'] != undefined) {
      this.rootFolderIDInThisView = '-1';
    }
    this.items = JSON.parse(JSON.stringify(this.folderTree[this.rootFolderIDInThisView]));
  }

  initFolderList() {
    this.lookingForFiles = true;
    this.dataService
      .getFolderList()
      .then(res => {
        this.folderList = res;
        this.buildTree();
        this.lookingForFiles = false;
      })
      .catch(() => {
        //
        this.lookingForFiles = false;
      });
  }

  sortByDate() {
    this.sortedByName = false;
    if (!this.dateArrowUp) {
      this.items.sort((a, b) => {
        let dateA = a.created_at;
        let dateB = b.created_at;
        if (dateA > dateB) {
          return 1;
        }
        if (dateA < dateB) {
          return -1;
        }
        return 0;
      });
    } else {
      this.items.sort((a, b) => {
        let dateA = a.created_at;
        let dateB = b.created_at;
        if (dateB > dateA) {
          return 1;
        }
        if (dateB < dateA) {
          return -1;
        }
        return 0;
      });
    }
    this.dateArrowUp = !this.dateArrowUp;
  }

  sortByName() {
    this.sortedByName = true;
    if (!this.nameArrowUp) {
      this.items.sort((a, b) => {
        let nameA = a.name.toUpperCase();
        let nameB = b.name.toUpperCase();
        if (nameA > nameB) {
          return 1;
        }
        if (nameA < nameB) {
          return -1;
        }
        return 0;
      });
    } else {
      this.items.sort((a, b) => {
        let nameA = a.name.toUpperCase();
        let nameB = b.name.toUpperCase();
        if (nameB > nameA) {
          return 1;
        }
        if (nameB < nameA) {
          return -1;
        }
        return 0;
      });
    }
    this.nameArrowUp = !this.nameArrowUp;
  }

  getItems(ev: any) {
    // Reset items back to all of the items
    this.items = JSON.parse(JSON.stringify(this.folderTree[this.rootFolderIDInThisView]));

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  search() {
    this.searchBar = !this.searchBar;
    if (!this.searchBar) {
      this.initFolderList();
    }
  }

  showObjectMenu(myEvent: any) {
    let popover = this.popoverCtrl.create(ObjectMenuPopover);
    popover.present({
      ev: myEvent
    });
  }

  addFolder() {
    let alert = this.alertCtrl.create({
      title: 'Nueva carpeta',
      inputs: [
        {
          name: 'folderName'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Crear',
          handler: data => {
            return data;
          }
        }
      ]
    });
    alert.present();
  }

  goToChild(id: string) {
    if (this.folderTree[id].length > 0) {
      this.parent = JSON.parse(JSON.stringify(this.folderList));
      this.rootFolderIDInThisView = id;
      this.folderList = this.folderTree[id];
      this.buildTree();
    }
  }

  goBack() {
    this.folderList = this.parent;
    this.buildTree();
  }
}
